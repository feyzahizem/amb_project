<?php

namespace VMB\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VMB\QuizBundle\Entity\NoteRepository")
 */
class Note
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
	* @ORM\ManyToOne(targetEntity="VMB\UserBundle\Entity\User")
	* @ORM\JoinColumn(nullable=false, onDelete="CASCADE") 
    */
    private $user;

    /**
	* @ORM\ManyToOne(targetEntity="VMB\PresentationBundle\Entity\Presentation")
	* @ORM\JoinColumn(nullable=false, onDelete="CASCADE") 
     */
    private $presentation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \VMB\UserBundle\Entity\User $user
     * @return Note
     */
    public function setUser($user)
    {
        $this->user = $user;

    }

    /**
     * Get user
     *
     * @return \VMB\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set presentation
     *
     * @param \VMB\PresentationBundle\Entity\Presentation $presentation
     * @return Note
     */
    public function setPresentation($presentation)
    {
        $this->presentation = $presentation;
    }

    /**
     * Get presentation
     *
     * @return \VMB\PresentationBundle\Entity\Presentation 
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Set question
     *
     * @param \VMB\QuizBundle\Entity\Question $question
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \VMB\QuizBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Note
     */
    public function setDate()
    {
        $this->date=new \DateTime("now");

    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
       
}
