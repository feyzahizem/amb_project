<?php

namespace VMB\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionNote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VMB\QuizBundle\Entity\QuestionNoteRepository")
 */
class QuestionNote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
	* @ORM\ManyToOne(targetEntity="VMB\QuizBundle\Entity\Note")
	* @ORM\JoinColumn(nullable=false, onDelete="CASCADE") 
    */
    private $note; 


    /**
	* @ORM\ManyToOne(targetEntity="VMB\QuizBundle\Entity\Question")
	* @ORM\JoinColumn(nullable=false, onDelete="CASCADE") 
    */
    private $question;

    /**
     * @var float
     *
     * @ORM\Column(name="noteObtenue", type="float")
     */
    private $noteObtenue;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get note
     *
     * @return float
     */
    public function getNoteObtenue()
    {
        return $this->noteObtenue;
    }

    /**
     * Get note
     *
     * @return \VMB\QuizBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    public function setNote($note){
        $this->note=$note;
    }

    public function setQuestion($question){
        $this->question=$question;
    }

    public function setNoteObtenue($note){
        $this->noteObtenue=$note;
    }

}
