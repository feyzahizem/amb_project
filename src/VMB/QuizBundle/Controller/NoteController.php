<?php

namespace VMB\QuizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VMB\QuizBundle\Entity\Note;

class NoteController extends Controller
{
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $entities = $em->getRepository('VMBQuizBundle:Note')->findAllByUser($user);
        //dump($entities);
        //$questions = $em->getRepository('VMBQuizBundle:QuestionNote')->findAllByDate(1);
        //dump($questions);
        return $this->render('VMBQuizBundle:Note:show.html.twig', array('entities' => $entities , 'em' => $em 
                // ...
            ));    }
    
    public function showNotesOwnerAction()
    {
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $entities = $em->getRepository('VMBPresentationBundle:Presentation')->findAllByOwner($user);
        return $this->render('VMBQuizBundle:Note:showNotesOwner.html.twig', array('entities' => $entities , 'em' => $em , 
            // ...
        ));    }

}
