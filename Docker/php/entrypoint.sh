#! /bin/bash

echo "Entering entrypoint ..."

if [ ! -d vendor/ ]; then
    echo "'vendor/' directory is missing, installing project"
    composer install
fi
